using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hamaca_Controller : Player_Controller 
{


    void Update()
    {
        if (Inmovilizado == true)
        {

            TiempoEsperaGota -= Time.deltaTime;
        }
        if (TiempoEsperaGota <= 0)
        {

            speed = 5;
            TiempoEsperaGota = 5;
            Inmovilizado = false;
        }

    }

    public override void OnCollisionEnter(Collision collision)
     {
       if(collision.gameObject.CompareTag("Techo"))
       {
        rb.useGravity = !rb.useGravity;

         HamacaEnTecho = true;
         Debug.Log("Toco techo;");

       }
        if (collision.gameObject.name == "Gota")
        {

            speed = 0;
            Inmovilizado = true;



        }
        if (collision.gameObject.name == "ArenaMovediza")
        {
            speed = 3;

        }
        else
        {
            speed = 15;
        }


    }

}
