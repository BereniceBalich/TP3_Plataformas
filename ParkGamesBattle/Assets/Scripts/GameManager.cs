using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    
    public Canvas_Controller canvas_Controller;
    public static int actualPlayer = 0;
    public List<Player_Controller> players;

    void Start()
    { 
       canvas_Controller.cambiarTxtJugador("Caballo");
       SetConstraits();
    }

    void Update()
    {
        mostrarTxtJugador();
       SetConstraits();
        GetInput();
        
    }

    public void mostrarTxtJugador()
    {
        if (actualPlayer == 0)
        {
            canvas_Controller.cambiarTxtJugador("Caballo");
        }
        
        else if (actualPlayer == 1)
        {
            canvas_Controller.cambiarTxtJugador("Hamaca");
        }
        
        else if (actualPlayer == 2)
        {
          canvas_Controller.cambiarTxtJugador("Pasamanos");
        }
        
        else if (actualPlayer == 3)
        {
            canvas_Controller.cambiarTxtJugador("Sube y baja");
        }
        
        else if (actualPlayer == 4)
        {
            canvas_Controller.cambiarTxtJugador("Tubo");
        }
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (actualPlayer <= 0)
            {
                actualPlayer = 4;
               SetConstraits(); 
            }
            else
            {
                actualPlayer--;
               SetConstraits();
            }
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (actualPlayer >= 4)
            {
                actualPlayer = 0;
                SetConstraits();
            }
            else
            {
                actualPlayer++;
               SetConstraits();
            }
        }
    }
    public void SetConstraits()
    {
        foreach(Player_Controller p in players)
        {
            
            if (p == players[actualPlayer] && actualPlayer != 3 && actualPlayer != 4 && actualPlayer != 1)
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
            }
            else if(p == players[actualPlayer] &&  actualPlayer == 3)
            { 
            p.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationY| RigidbodyConstraints.FreezeRotationZ;
            }
            else if(p == players[actualPlayer] &&  actualPlayer == 4 && p.TuboPuedeSubir == false )
            { 
             p.rb.constraints = RigidbodyConstraints.FreezePositionX  | RigidbodyConstraints.FreezeRotation;
            }
            else if(p == players[actualPlayer] &&  actualPlayer == 4 && p.TuboPuedeSubir == true )
            { 
              p.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
            }
            else if(p == players[actualPlayer] &&  actualPlayer == 1 && p.HamacaEnTecho == false)
            { 
            p.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
            }
            else if(p == players[actualPlayer] &&  actualPlayer == 1 && p.HamacaEnTecho == true )
            {  
             
              p.rb.constraints = RigidbodyConstraints.FreezePosition |RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            }
            
            else 
            {
                p.rb.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
            }
        }
    }

    

   

}
