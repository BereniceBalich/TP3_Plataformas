using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{  
    public GameObject Enemigo1Obj;
     public bool tuboPuedeSubir = false;
    //Variables de hamaca
    public bool hamacaEnTecho = false;
    public float rotationSpeed = 45f; // Velocidad de oscilación en grados por segundo
    public float rotationAngle = 60; // Ángulo máximo de oscilación en grados
    private float currentAngle = 0f; // Ángulo actual de oscilación
    private bool isForward = true; // Bandera para indicar la dirección de oscilación
    public int jumpForce = 1;
    public int jumpForceCaballo = 10;
    //Variables para los jugadores en general
    public int speed = 15;
    public int playerNumber;
    public Rigidbody rb;
    public float rotacion = 0;
    //GameObjects y transform de los jugadores
    public GameObject caballoObj;
    public GameObject hamacaObj;
    public GameObject pasamanosObj;
    public GameObject subeYbajaObj;
    public GameObject tuboObj;
    public Transform transformCaballo;
    public Transform transformHamaca;
    public Transform transformPasamanos;
    public Transform transformSubeYbaja;
    public Transform transformTubo;
    //Varaibles para el tubo
    public bool cGuardado = false;
    public bool hGuardada= false;
    public bool pGuardado= false;
    public bool sYbGuardado= false;
    
    public  bool TuboPuedeSubir
    {
        get { return tuboPuedeSubir; }
        set { tuboPuedeSubir = value; }
    }
    public bool HamacaEnTecho
    {
        get { return hamacaEnTecho; }
        set { hamacaEnTecho = value; }
    }

    //variables para el enemigo gota
    public float TiempoEsperaGota = 5f;
    public bool Inmovilizado = false;





    public void Start()
    {
        //Codigo que puede servir mas adelante:
        // Invoca el método de rotación repetidamente cada 'rotationInterval' segundos
        //InvokeRepeating("RotateObject", 0f, rotationInterval);


        TuboPuedeSubir = false;
        transformCaballo = caballoObj.GetComponent<Transform>(); 
        transformHamaca = hamacaObj.GetComponent<Transform>(); 
        transformPasamanos = pasamanosObj.GetComponent<Transform>(); 
        transformSubeYbaja = subeYbajaObj.GetComponent<Transform>(); 
        transformTubo= tuboObj.GetComponent<Transform>(); 
        rb = GetComponent<Rigidbody>();
    }
     
    public virtual void FixedUpdate()
    {
    
       activadorDeObj();
         SetMovement();
        
         if(HamacaEnTecho == true && Inmovilizado == false)
        {
          float deltaAngle = rotationSpeed * Time.deltaTime * (isForward ? 4f : -4f);

          // Calcula el nuevo ángulo
         currentAngle += deltaAngle;
 
        // Si el ángulo alcanza el límite, cambia la dirección de oscilación
         if (Mathf.Abs(currentAngle) >= rotationAngle)
         {
            isForward = !isForward;
         }

         // Aplica la rotación en el eje X al objeto de la hamaca
          transformHamaca.rotation = Quaternion.Euler(currentAngle, transformHamaca.rotation.eulerAngles.y, transformHamaca.rotation.eulerAngles.z);
        }
    }
    void Update()
    {

        

     
    } 


    public void activadorDeObj()
    {
        if(Input.GetKey(KeyCode.F) && sYbGuardado == true)
         {
            Vector3 newPosition = transformTubo.position + new Vector3(0 , 0 , +5 );
            subeYbajaObj.gameObject.SetActive(true);
             transformSubeYbaja.position = new Vector3(transformSubeYbaja.position.x ,  transformSubeYbaja.position.y , transformTubo.position.z +5);
            sYbGuardado = false;
         }
            if(Input.GetKey(KeyCode.F) && pGuardado == true)
         {
            Vector3 newPosition2 = transformTubo.position + new Vector3(0, 0, +10);
            pasamanosObj.gameObject.SetActive(true);
            transformPasamanos.position = new Vector3(transformPasamanos.position.x ,  transformPasamanos.position.y , transformTubo.position.z - 2);
            pGuardado = false;
         }
          if(Input.GetKey(KeyCode.F) && hGuardada == true)
         {
            Vector3 newPosition3 = transformTubo.position + new Vector3(0 , 0 , +5 );
            hamacaObj.gameObject.SetActive(true);
           transformHamaca.position = new Vector3(transformHamaca.position.x ,  transformHamaca.position.y , transformTubo.position.z  +5);
            hGuardada = false;
         } 
         if(Input.GetKey(KeyCode.F) && cGuardado == true)
         {
            Vector3 newPosition = transformTubo.position + new Vector3(0 , 0 , +5 );
            caballoObj.gameObject.SetActive(true);
            transformCaballo.position = new Vector3(transformCaballo.position.x ,  transformCaballo.position.y , transformTubo.position.z  +1);
            cGuardado = false;
         }
    }
    public void SetMovement()
    {

        if (GameManager.actualPlayer == 0)
        {
           
           
                rb.useGravity = true;
                caballoMovement();

            
           
        }
        
        else if (GameManager.actualPlayer == 1)
        {
            
               hamacaMovement();
            
           
        }
        
        else if (GameManager.actualPlayer == 2)
        {
          
         
                rb.useGravity = true;
              basicMovement();
            
           
        }
        
        else if (GameManager.actualPlayer == 3)
        {
            
                rb.useGravity = true;
               SubeYbajaMov();
            
           
        }
        
        else if (GameManager.actualPlayer == 4)
        {
          
            
                rb.useGravity = true;
                tuboMovement();
            
            
        }

    }

   
   
     public void basicMovement()
    {
        if (Input.GetKey(KeyCode.A))
        {
                rb.velocity = new Vector3( 0 , rb.velocity.y, 1 * -speed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
                rb.velocity = new Vector3(0, rb.velocity.y, 1 * speed);
        }
    }
     public void caballoMovement()
    {
        if (Input.GetKey(KeyCode.A))
        {
                rb.velocity = new Vector3( 0 , rb.velocity.y, 1 * -speed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
                rb.velocity = new Vector3(0, rb.velocity.y, 1 * speed);
        }
        else if (Input.GetKey(KeyCode.W) )
        {
                rb.velocity = new Vector3(rb.velocity.x, 1 * jumpForceCaballo, 0);
        }

    }
    public void tuboMovement()
    {
        if (Input.GetKey(KeyCode.A))
        {
                rb.velocity = new Vector3( 0,rb.velocity.y, 1 * -speed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            
                rb.velocity = new Vector3( 0 ,rb.velocity.y, 1 * speed);
        }
        else if (Input.GetKey(KeyCode.W) && TuboPuedeSubir == true )
        {
                rb.velocity = new Vector3(rb.velocity.x, 1 * speed, 0);
        }
    }
    
    public void hamacaMovement()
    {
        if (Input.GetKey(KeyCode.A))
        {
                rb.velocity = new Vector3( 0,rb.velocity.y, 1 * -speed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
                rb.velocity = new Vector3( 0 ,rb.velocity.y, 1 * speed);
        }
        else if (Input.GetKey(KeyCode.W) && HamacaEnTecho == false)
        {
              rb.useGravity = !rb.useGravity;
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
        else if (Input.GetKey(KeyCode.S) && HamacaEnTecho == true)
        {
            HamacaEnTecho = false;
            rb.useGravity = true;
            transformHamaca.rotation = Quaternion.Euler(0,0, 0);
        }
        

    }

    public void SubeYbajaMov()
    {
        rotacion = rb.transform.localRotation.x;

        if (Input.GetKey(KeyCode.A) )
        {
                rb.velocity = new Vector3( 0 , rb.velocity.y, 1 * -speed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
                rb.velocity = new Vector3(0, rb.velocity.y, 1 * speed);
        }
        else if(Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W) )
        {
           rotacion = +5;
        }
        else if(Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W) )
        {
           rotacion = -5;
        }
       

    }
    public virtual void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.name == "ArenaMovediza")
        {
            speed = -10;

        }


    }
    public virtual void OnCollisionExit(Collision collision){}
    public virtual void OnCollisionStay(Collision collision)
    {
    
    }

}
