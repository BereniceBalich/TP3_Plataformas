using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Canvas_Controller : MonoBehaviour
{
    public TMP_Text txtMostarJugadores;
    public GameObject CanvasInicio;
    public GameObject CanvasJugadores;


     public void btnClick()
    {
        Debug.Log("BUTTON PRESSED: "+ this.name);
        if (this.name == "btnInicio")
        {
            CanvasInicio.SetActive(false);
            CanvasJugadores.SetActive(true);

        }
    }

   public void cambiarTxtJugador(string nuevoJugador)
   {
     txtMostarJugadores.text = nuevoJugador;
   }
}
