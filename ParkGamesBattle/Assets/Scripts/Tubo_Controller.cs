using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tubo_Controller : Player_Controller
{
 
   

    void Update()
    {
        if(Inmovilizado == true)
        {
            
            TiempoEsperaGota -= Time.deltaTime;
        }
        if(TiempoEsperaGota <=0)
        {

            speed = 5;
            TiempoEsperaGota = 5;
            Inmovilizado = false;
        }

    }
    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Gota")
        {
            
            speed = 0;
            Inmovilizado = true;
           


        }
        if (Input.GetKey(KeyCode.T) && Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.T) && Input.GetKey(KeyCode.D))
       {
        if (collision.gameObject == subeYbajaObj)
        {
         Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
          subeYbajaObj.SetActive(false);
          sYbGuardado = true;
        }
        if (collision.gameObject == pasamanosObj)
        {
          Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
          pasamanosObj.SetActive(false);
          pGuardado = true;
        }
        if (collision.gameObject == hamacaObj)
        {
          Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
          hamacaObj.SetActive(false);
          hGuardada = true;
        }
         if (collision.gameObject == caballoObj)
        {
          Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
          caballoObj.SetActive(false);
          cGuardado = true;
        }
           



        }
       
    }
     public override void OnCollisionStay(Collision collision)
    {
      
        if( collision.gameObject == pasamanosObj )
        {
        
            TuboPuedeSubir = true;
            Debug.Log("TuboPuedeSubir = true;");
          
          
        }
        

    }
     public override void OnCollisionExit(Collision collision)
    {
        if( collision.gameObject == pasamanosObj )
        {
        
          TuboPuedeSubir = false;
          Debug.Log("TuboPuedeSubir = False;");
          
          
        }
       

    }
}
