using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caballo_Controller : Player_Controller
{



    void Update()
    {
        if (Inmovilizado == true)
        {

            TiempoEsperaGota -= Time.deltaTime;
        }
    
        if (TiempoEsperaGota <= 0)
        {

            speed = 5;
            TiempoEsperaGota = 5;
            Inmovilizado = false;
        }

    }
    public override void OnCollisionEnter(Collision collision)
     {
        if (collision.gameObject == Enemigo1Obj)
        {

            Debug.Log("Choco con enemigo");
            Enemigo1Obj.gameObject.SetActive(false);
        }
     
        if (collision.gameObject.name == "Gota")
        {

            speed = 0;
            Inmovilizado = true;



        }


    }
    public virtual void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name == "ArenaMovediza")
        {
            speed = -10;
        }
        else
        {
            speed = 15;
        }
     




    }

}
